#!/bin/bash

set -e -x

# ------ Parameters to set -----
# The location fo the EIGEN headers should be specified by passing the ENV 
# variable EIGEN_INCL to this script.  For example:
#
#$ EIGEN_INCL="${HOME}/Documents" sh ./script_build_windows.sh

# Specify build host (cross compile to win64)
HOST=x86_64-w64-mingw32
# -------------------------------

# This script assumes that a statically compiled version of fftw3 has already
# been installed in the host distribution.  see:
# https://gitlab.com/-/snippets/4809667

BUILD_DIR="build_$HOST"
# Remove previously existing build dir
if [ -d $BUILD_DIR ] ; then
    echo "Wiping previous build directory"
    rm -rf $BUILD_DIR
fi
# Create build directory
mkdir $BUILD_DIR; cd $BUILD_DIR

HOST="x86_64-w64-mingw32"
PREFIX="/usr/$HOST"

# Build static library
../configure --host=$HOST --prefix=$PREFIX  \
    --disable-shared --enable-static \
    CPPFLAGS="-I$EIGEN_INCL"

make


