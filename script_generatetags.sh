#!/bin/bash

# For YCM to work, we need ctags geenrated from the exuberant ctags package.
# The following command will generate the tags

ctags -R --fields=+l
