#!/bin/bash

set -ex

# Download FFTW3 source code
FILE="fftw-3.3.10.tar.gz"
if [ ! -f $FILE ]; then
    URL="https://www.fftw.org/$FILE"
    wget $URL
fi

# Extract contents
DIR=$(basename $FILE .tar.gz)
if [ ! -d $DIR ]; then
    tar -xvzf $FILE
fi 

# Move into extracted folder
cd $DIR 

# Make build directory

if [ ! -d build ]; then  
    mkdir build
fi 
cd build

# Set windows 64-bit cross compiler
# you need mingw-w64 installed for this
HOST=x86_64-w64-mingw32
PREFIX=/usr/${HOST}

# Build a cross-compiled static library targeting win64 machines
../configure --enable-shared=no --disable-doc --prefix=${PREFIX} --host=${HOST}
make all

# Command to install to minggw (cross compilation directory)
# sudo make install
