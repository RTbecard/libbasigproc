#!/bin/bash

set -e -x

echo "--- Making a fresh build directory"
BUILD_DIR="build"
# Remove previously existing build dir
if [ -d $BUILD_DIR ] ; then
    echo "Wiping previous build directory"
    rm -rf $BUILD_DIR
fi
# Create build directory
mkdir $BUILD_DIR; cd $BUILD_DIR

echo "--- Adding Rtools compiler to PATH.."
export PATH=/x86_64-w64-mingw32.static.posix/bin:$PATH
echo "$PATH"

echo "--- Configuring makefiles..."
../configure --disable-shared --enable-static 
make 
#make  LDFLAGS="-no-undefined"
echo "--- installing libbasigproc..."
make install
echo "--- Build and installation complete!"
