#!/bin/bash

set -e 

# This script will build the configure and make.in files
autoreconf --install --force
