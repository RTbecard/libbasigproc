#include <vector>
#include <complex>
#include <fftw3.h>
#include <numeric>
#include <eigen3/Eigen/Core>

#include "../include/libbasigproc.h"
#include "./macros.h"

using std::vector;
using std::complex;

// Setup type aliases
using cmplx = complex<double>;
using vcmplx = vector<complex<double>>;
using vdbl = vector<double>;

using Eigen::MatrixXd;
using Eigen::VectorXcd;


namespace basigproc
{

  std::vector<double> xcorr(vdbl &signal, vdbl kernel, int chunk_size, bool enveloped, bool standardized){

    int n_pad = kernel.size();

    // Init output vector
    vdbl ret(signal.size());
    copy(signal.begin(), signal.end(), ret.begin());

    // Create signal buffer object for looping over signal
    vdbl buffer(chunk_size);
    basigproc::SignalBuffer sig_buf(ret, buffer, n_pad);

    // Convert Kernel to analytic envelope
    if(enveloped) analytic_envelope(kernel);

    // Create padded kernel vector (copy it in reverse)
    vcmplx ckernel_pad(chunk_size);
    copy(kernel.begin(), kernel.end(), ckernel_pad.rbegin());
    fill(ckernel_pad.rbegin() + kernel.size(), ckernel_pad.rend(), cmplx(0,0));

    DEBUG_VECTOR(kernel.begin(), kernel.end());
    DEBUG_VECTOR(ckernel_pad.begin(), ckernel_pad.end());

    // Create padded rectangle window
    vcmplx crect_pad(chunk_size);
    fill(crect_pad.rbegin(), crect_pad.rbegin() + kernel.size(), cmplx(1, 0));
    fill(crect_pad.rbegin() + kernel.size(), crect_pad.rend(), cmplx(0, 0));

    // Convert padded kernel to frequency domain
    DEBUG_MESSAGE("Convert padded kernel to frequency domain");
    fftw_plan plan_kernel = fftw_plan_dft_1d(chunk_size, 
        reinterpret_cast<fftw_complex*>(ckernel_pad.data()),
        reinterpret_cast<fftw_complex*>(ckernel_pad.data()), 
        FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(plan_kernel);
    fftw_destroy_plan(plan_kernel);

    DEBUG_VECTOR(ckernel_pad.begin(), ckernel_pad.end());

    // Convert rectangle window to frequency domain
    DEBUG_MESSAGE("Convert padded rectangle to frequency domain");
    fftw_plan plan_rect = fftw_plan_dft_1d(chunk_size, 
        reinterpret_cast<fftw_complex*>(crect_pad.data()),
        reinterpret_cast<fftw_complex*>(crect_pad.data()), 
        FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(plan_rect);
    fftw_destroy_plan(plan_rect);

    DEBUG_VECTOR(crect_pad.begin(), crect_pad.end());

    // Calculate the root-sum square of the kernel
    double rss_kernel = 0;
    for(double i: kernel){
      rss_kernel += pow(double(i),double(2));
    }
    rss_kernel = pow(rss_kernel, double(0.5));
    DEBUG_VALUE(rss_kernel);

    // Init temporary containers
    vcmplx cbuffer(chunk_size);
    vcmplx buffer_cnv_ckern(chunk_size);
    vcmplx buffer_cnv_crect(chunk_size);
    vdbl buffer_cvn_rect(chunk_size);

    // ----- Init FFTW plans
    DEBUG_MESSAGE("Init FFTW plans");
    // cbuffer
    fftw_plan plan_cbuff_t_to_f = fftw_plan_dft_1d(chunk_size, 
        reinterpret_cast<fftw_complex*>(cbuffer.data()),
        reinterpret_cast<fftw_complex*>(cbuffer.data()),
        FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_plan plan_cbuff_f_to_t = fftw_plan_dft_1d(chunk_size, 
        reinterpret_cast<fftw_complex*>(cbuffer.data()),
        reinterpret_cast<fftw_complex*>(cbuffer.data()),
        FFTW_BACKWARD, FFTW_ESTIMATE);
    // Kernel convolution 
    fftw_plan plan_kern_t_to_f= fftw_plan_dft_1d(chunk_size, 
        reinterpret_cast<fftw_complex*>(cbuffer.data()),
        reinterpret_cast<fftw_complex*>(buffer_cnv_ckern.data()),
        FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_plan plan_kern_f_to_t= fftw_plan_dft_c2r_1d(chunk_size, 
        reinterpret_cast<fftw_complex*>(buffer_cnv_ckern.data()),
        buffer.data(), FFTW_ESTIMATE);
    // Rectangle convlution
    fftw_plan plan_rect_f_to_t= fftw_plan_dft_c2r_1d(chunk_size, 
        reinterpret_cast<fftw_complex*>(cbuffer.data()),
        buffer_cvn_rect.data(), FFTW_ESTIMATE);

    vdbl::iterator itr_buf, itr_rect;
    vcmplx::iterator itr_cbuf, itr_ckern, itr_crect;

    double dft_scale = standardized ? pow(chunk_size, double(0.5)) : pow(chunk_size, double(1));
    // this holds the scaling for all the combined DFT operations
    DEBUG_VALUE(dft_scale);

    // Start looping through signal buffer
    DEBUG_MESSAGE("Start looping through signal buffer");
    while(sig_buf.next()){

      DEBUG_VALUE(sig_buf.idx_chunk);

      // Fill complex buffer
      itr_cbuf = cbuffer.begin();
      itr_buf = buffer.begin();
      for(; itr_cbuf != cbuffer.end(); ++itr_buf, ++itr_cbuf)
        *itr_cbuf = cmplx(*itr_buf, double(0));
      
      // Get analytic envelope
      if(enveloped){
        // To frequency domain
        fftw_execute(plan_cbuff_t_to_f); 
        // Get analytic signal
        analytic_signal(cbuffer);
        // Return to time domain
        fftw_execute(plan_cbuff_f_to_t);
        // Convert to envelope function
        for(auto itr = cbuffer.begin(); itr != cbuffer.end(); ++itr)
          *itr = cmplx(abs(*itr), 0);
      }
      
      // ------ Convolve buffer with kernel
      DEBUG_MESSAGE("Convolve buffer with kernel");
      // Convert cbuffer to frequency domain
      fftw_execute(plan_kern_t_to_f); 
      // Convolve with reverse kernel
      itr_ckern = ckernel_pad.begin();
      for(itr_cbuf = buffer_cnv_ckern.begin(); itr_cbuf != buffer_cnv_ckern.end(); ++itr_ckern, ++itr_cbuf)
        *itr_cbuf *= *itr_ckern; 
      // Return to real-valued time-domain
      fftw_execute(plan_kern_f_to_t);

      if(standardized){
        // ------ Convolve squared buffer with rectangle window
        DEBUG_MESSAGE("Convolve squared buffer with rectangle window");
        // Square buffer
        for(auto itr = cbuffer.begin(); itr != cbuffer.end(); ++itr)
          itr->real(pow(itr->real(), 2));
        // only apply to real parts, as imaginary is all set to 0
        // Transform to frequency domain
        fftw_execute(plan_cbuff_t_to_f); 
        // Convolve with rectangle window
        itr_crect = crect_pad.begin();
        for(auto itr_cbuf = cbuffer.begin(); itr_cbuf != cbuffer.end(); ++itr_crect, ++itr_cbuf)
          *itr_cbuf *= *itr_crect; 
        // Convert back to time-domain
        fftw_execute(plan_rect_f_to_t);

        // ------ Compute pearsons correlation values
        DEBUG_MESSAGE("Computer Pearsons correlation coefficients");
        itr_rect = buffer_cvn_rect.begin();
        for(itr_buf = buffer.begin(); itr_buf != buffer.end(); ++itr_buf, ++itr_rect)
          *itr_buf /= pow(*itr_rect, double(0.5)) * rss_kernel; 
      }

      // Apply DFT scaling
      DEBUG_MESSAGE("Apply DFT scaling to output");
      for(itr_buf = buffer.begin(); itr_buf != buffer.end(); ++itr_buf)
        *itr_buf /= dft_scale; 

    }

    fftw_destroy_plan(plan_cbuff_t_to_f);
    fftw_destroy_plan(plan_cbuff_f_to_t);
    fftw_destroy_plan(plan_kern_t_to_f);
    fftw_destroy_plan(plan_kern_f_to_t);
    fftw_destroy_plan(plan_rect_f_to_t);
    
    return ret;
  }
}


