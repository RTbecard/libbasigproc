#include <vector>
#include <complex>
#include <fftw3.h>
#include <numeric>
#include <eigen3/Eigen/Core>
#include <vector>
#include "../include/libbasigproc.h"
#include "./macros.h"

using std::vector;
using std::complex;
using cmplx = complex<double>;
using vdbl = vector<double>;
using vcmplx = vector<complex<double>>;

namespace basigproc{

  void analytic_signal(vcmplx &freq_response){
    // Multiply positive frequencies by 2
    for(auto i = freq_response.begin() + 1;
        i != freq_response.begin() + ceil(freq_response.size()/2) - 1; ++i)
      *i *= 2;
    // Remove negative frequencues
    for(auto i = freq_response.begin() + ceil(freq_response.size()/2); 
        i != freq_response.end(); ++i)
      *i = 0;
  }

  vcmplx analytic_signal(vdbl &signal){


    // Init output vector
    vcmplx out(signal.size());

    // Fill complex vector with signal
    auto itr_sig = signal.begin();
    for(auto itr = out.begin(); itr != out.end(); ++itr, ++itr_sig)
      *itr = cmplx(*itr_sig, 0);

    // Apply DFT
    fftw_plan plan_t_to_f = fftw_plan_dft_1d(signal.size(), 
        reinterpret_cast<fftw_complex*>(out.data()),
        reinterpret_cast<fftw_complex*>(out.data()), FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(plan_t_to_f);
    fftw_destroy_plan(plan_t_to_f);
    DEBUG_VECTOR(out.begin(), out.end());

    // Create analytic signal
    DEBUG_MESSAGE("Create analytic signal");
    analytic_signal(out);

    // Apply dft scaling
    for(auto i = out.begin(); i != out.begin() + ceil(out.size()/2); ++i)
      *i /= signal.size();

    DEBUG_VECTOR(out.begin(), out.end());

    // Convert back to complex time-domain
    fftw_plan plan_f_to_t = fftw_plan_dft_1d(signal.size(), 
        reinterpret_cast<fftw_complex*>(out.data()),
        reinterpret_cast<fftw_complex*>(out.data()), FFTW_BACKWARD, FFTW_ESTIMATE);
    fftw_execute(plan_f_to_t);
    fftw_destroy_plan(plan_f_to_t);

    return out;

  }

  void analytic_envelope(vdbl &signal){

    vcmplx anly_sig = analytic_signal(signal);
    vcmplx::iterator itr_anly = anly_sig.begin();

    for(auto itr = signal.begin(); itr != signal.end(); ++itr, ++itr_anly)
      *itr = abs(*itr_anly);

  } 
}
