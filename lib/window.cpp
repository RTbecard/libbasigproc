#include <cmath>
#include <vector>

#include "./macros.h"
#include "../include/libbasigproc.h"

// Load math constants
#define _USE_MATH_DEFINES

using std::vector;


namespace basigproc{
  vector<double> hamming(int n){

    // Init vector
    vector<double> out(n);
    
    double a = double(25)/double(46);
    for(int i = 0; i != n; ++i)
      out[i] = a - ((1-a) * std::cos((2*M_PI*i) / (n-1)));

    return out;
  }


  vector<double> hann(int n){

    // Init vector
    vector<double> out(n);

    double a = double(0.5);
    for(int i = 0; i != n; ++i)
      out[i] = a - ((1-a) * std::cos((2*M_PI*i) / (n-1)));

    return out;
  }
}
