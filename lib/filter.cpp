// include complex before fftw
#include <complex>
#include <fftw3.h>
#include <iostream>
#include <vector>

#include <macros.h>

#include "../include/libbasigproc.h"

using std::vector;
using std::complex;

using vdbl = vector<double>;
using vcmplx = vector<complex<double>>;

namespace basigproc{

  void filter(vector<double> &signal, 
      vector <double> &impulse, int chunk_size, 
      bool filtfilt){

    // Size of one-sided FFT results
    const int chunk_size_freq = floor(chunk_size / 2) + 1;
    // Pad each side of the buffer to the length of the impulse response
    const int n_pad = impulse.size();
    // Init buffer vector (we will modify the data in this buffer)
    vdbl buffer(chunk_size);
    // Init the object for managing the signal buffer
    SignalBuffer sig_buff(signal, buffer, n_pad);

    // --- Calculate the calibration frequency response
    // Create a new impulse responses that is zero-padded to match the chunk
    // size
    vdbl impulse_pad(chunk_size);
    std::copy(impulse.begin(), impulse.end(), impulse_pad.begin());
    std::fill(impulse_pad.begin() + impulse.size(), impulse_pad.end(), 0);


    DEBUG_MESSAGE("convert impulse to on-sided spectrum");

    // --- Convert impulse response to one-sided spectrum
    vcmplx freq_resp(chunk_size_freq);
    vcmplx::iterator itr_freq_resp;
    fftw_plan fftplan_impulse = fftw_plan_dft_r2c_1d(chunk_size, 
        impulse_pad.data(),
        reinterpret_cast<fftw_complex*>(freq_resp.data()),
        FFTW_ESTIMATE);
    fftw_execute(fftplan_impulse);
    fftw_destroy_plan(fftplan_impulse);

    DEBUG_VECTOR(freq_resp.begin(), freq_resp.begin()+10);
    DEBUG_VECTOR(freq_resp.end()-10, freq_resp.end());

    // Initialize buffer for DFT results
    vcmplx buffer_freq(chunk_size_freq);
    vcmplx::iterator itr_buf_freq;

    DEBUG_MESSAGE("Init plan for signal FFT");

    // Initalize plan to convert buffer to frequency domain
    fftw_plan fftplan_buffer_t_to_f = fftw_plan_dft_r2c_1d(chunk_size, 
        buffer.data(),
        reinterpret_cast<fftw_complex*>(buffer_freq.data()),
        FFTW_ESTIMATE);

    // Initalize plan to convert buffer back to time domain
    fftw_plan fftplan_buffer_f_to_t = fftw_plan_dft_c2r_1d(chunk_size, 
        reinterpret_cast<fftw_complex*>(buffer_freq.data()),
        buffer.data(), FFTW_ESTIMATE);

    DEBUG_MESSAGE("Start looping over signal chunks");


    // Loop through the signal chunks
    while(sig_buff.next()){

      // --- Convert buffer to frequency domain ---
      DEBUG_MESSAGE("Convert buffer to frequency domain (buffer_freq)");
      fftw_execute(fftplan_buffer_t_to_f);

      // --- Apply convolution with impulse response
      DEBUG_MESSAGE("Convolve with impulse response");
      // Multiply the buffer and impulse respose in the frequnecy domain
      itr_freq_resp = freq_resp.begin();
      itr_buf_freq = buffer_freq.begin();
      for(; itr_buf_freq != buffer_freq.end(); ++itr_buf_freq, ++itr_freq_resp){
        *itr_buf_freq *= *itr_freq_resp; 

        // For filtfilt, multiplying by the complex conjugate of the frequency
        // response is equivalent to convolution with a time-reversed signal
        if(filtfilt) *itr_buf_freq *= std::conj(*itr_freq_resp);
      }

      // --- Copy convolution result back to the time-domain buffer ---
      DEBUG_MESSAGE("Convert convolved signal back to time domain");
      // This will destroy contents of buffer_freq
      fftw_execute(fftplan_buffer_f_to_t);

      // Apply DFT scaling
      for(auto itr = buffer.begin(); itr != buffer.end(); ++itr)
        *itr *=  double(1) / double(chunk_size); 
    }

    fftw_destroy_plan(fftplan_buffer_f_to_t);
    fftw_destroy_plan(fftplan_buffer_t_to_f);

  }
}
