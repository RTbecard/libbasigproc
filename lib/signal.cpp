#include <complex>
//#include <fftw3.h>
#include <vector>

extern "C" {
  #include <fftw3.h>
}

#include "../include/libbasigproc.h"


// Class for holding time-domain signals
basigproc::SignalTime::SignalTime(
    int chan_x, int chan_y, int chan_z, int chan_o, 
    double fs, std::vector<double> &data, 
    int calibrated = SIGNAL_RAW): data(data)
/* 
 * The member data is a reference which means it must be initialized when
 * delcared. We have to use a constructor intializer list for this.
 */
{
  this->chan_x = chan_x;
  this->chan_y = chan_y;
  this->chan_z = chan_z;
  this->chan_o = chan_o;
  this->calibrated = calibrated;
  this->fs = fs;
  this->n = data.size();
}
// Class for holding frequency domain signals
// Here, we hold the 1-sided spectrum
basigproc::SignalFreq::SignalFreq(
    int chan_x, int chan_y, int chan_z, int chan_o, 
     int n, double fs, std::vector<fftw_complex> &data, 
    int calibrated = SIGNAL_RAW): data(data)
{
  this->chan_x = chan_x;
  this->chan_y = chan_y;
  this->chan_z = chan_z;
  this->chan_o = chan_o;
  this->fs = fs;
  this->n = data.size()*2 + 1;
}
