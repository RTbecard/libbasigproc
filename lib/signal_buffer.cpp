// include complex before fftw
#include <iostream>
#include <vector>

#include <macros.h>

#include "../include/libbasigproc.h"

using std::vector;
using vdbl = vector<double>;

namespace basigproc{

  // Constructor
  SignalBuffer::SignalBuffer(vdbl &signal, vdbl &buffer, int n_pad):
    signal(signal), buffer(buffer), n_pad(n_pad){

      size_chunk = buffer.size();
      idx_chunk = 0;
      // Init intermediate buffer
      // This stores the modified buffer before it is copied back to the
      // signal and while the buffer is refreshed.
      buffer_intr = vdbl(size_chunk); 

      // The inner buffer size is the number of buffer samples that will be
      // copied back into the singal vector (i.e. buffer size minus padding)
      size_chunk_inner = size_chunk - n_pad*2;

      // Find the number of chunks
      // Round up so final chunk goes past the end of the signal
      n_chunk = std::ceil(signal.size() / double(size_chunk_inner));

      // Error checking
      if(size_chunk_inner < 0)
        throw std::invalid_argument("[libbasigproc::SignalBuffer] buffer is too small for n_pad values.");
      if(size_chunk_inner < n_pad){
        std::cerr << "[libbasigproc::SignalBuffer] WARNING: The length of the signal data in the buffer is smaller than the padding.";
        std::cerr << "It is reccomended to increase the chunk_size." << std::endl;
      }

      DEBUG_VALUE(n_pad);
      DEBUG_VALUE(n_chunk);
      DEBUG_VALUE(size_chunk);
      DEBUG_VALUE(size_chunk_inner);
      DEBUG_VALUE(buffer.size());
      DEBUG_VALUE(buffer_intr.size());
      DEBUG_VALUE(signal.size());

    };

  bool SignalBuffer::next(){

    // Load next chunk into buffer & copy previous buffer back into signal
    DEBUG_VALUE(idx_chunk);

    if(idx_chunk > 0){
      DEBUG_MESSAGE(" --- Copy buffer to buffer_intr");
      DEBUG_VECTOR(buffer.begin(), buffer.end());
      DEBUG_VECTOR(buffer_intr.begin(), buffer_intr.end());
      // Copy buffer to buffer_intr
      copy(buffer.begin(), buffer.end(), buffer_intr.begin());
    }

    // Load new signal chunk into buffer
    if(idx_chunk < n_chunk){
      DEBUG_MESSAGE(" --- Read in new signal chunk");
      read_signal_chunk();
      DEBUG_VECTOR(buffer.begin(), buffer.end());
    }

    // Copy buffer_intr back into previous signal chunk
    if(idx_chunk > 0){
      // get startn end end indicies for writing buffer back to signal
      idx_chunk_bufintr_start = size_chunk_inner*(idx_chunk-1);
      idx_chunk_bufintr_end = idx_chunk_bufintr_start + size_chunk_inner < signal.size() ?
        idx_chunk_bufintr_start + size_chunk_inner :
        signal.size();

      DEBUG_MESSAGE(" --- Copy buffer_intr back to signal");
      DEBUG_VALUE(idx_chunk_bufintr_start);
      DEBUG_VALUE(idx_chunk_bufintr_end);
      copy(buffer_intr.begin() + n_pad, 
          buffer_intr.begin() + n_pad + idx_chunk_bufintr_end - idx_chunk_bufintr_start, 
          signal.begin() + idx_chunk_bufintr_start);
      // iterate signal chunk index
    }

    // Increment chunk counter

    return idx_chunk++ < n_chunk;

  };

  void SignalBuffer::read_signal_chunk(){


    // set iterators to the start end endo of signal chunk to extract
    idx_chunk_buf_start = size_chunk_inner*idx_chunk;
    idx_chunk_buf_end = idx_chunk_buf_start + size_chunk_inner < signal.size() ?
      idx_chunk_buf_start + size_chunk_inner :
      signal.size(); 
    // reset buffer iterator
    itr_buf = buffer.begin();

    DEBUG_VALUE(idx_chunk_buf_start);
    DEBUG_VALUE(idx_chunk_buf_end);

    DEBUG_MESSAGE(" --- Copy left padding ");
    DEBUG_VALUE(itr_buf - buffer.begin());

    // --- Copy left padding
    if(idx_chunk_buf_start == 0)
      // First chunk
      for(itr_sig = signal.begin() + n_pad;
          itr_buf != buffer.begin() + n_pad;
          ++itr_buf, --itr_sig)
        // padding is the negative mirror image of the begining of signal
        *itr_buf = -*itr_sig;
    else
      // Intermediate chunk
      for(itr_sig = signal.begin() + idx_chunk_buf_start - n_pad;
          itr_buf != buffer.begin() + n_pad;
          ++itr_buf, ++itr_sig)
        // padding uses previous chunk 
        *itr_buf = *itr_sig;


    DEBUG_MESSAGE(" --- Copy inner data");
    DEBUG_VALUE(itr_buf - buffer.begin());

    // --- Copy inner data
    for(itr_sig = signal.begin() + idx_chunk_buf_start;
        itr_sig != signal.begin() + idx_chunk_buf_end;
        ++itr_buf, ++itr_sig)
      *itr_buf = *itr_sig;


    DEBUG_MESSAGE(" --- Copy right padding (real data)");
    DEBUG_VALUE(itr_buf - buffer.begin());

    // --- Copy next chunk to right buffer
    for(itr_sig = signal.begin() + idx_chunk_buf_end;
        itr_sig != signal.end() && itr_buf != buffer.end();
        ++itr_buf, ++itr_sig){
      *itr_buf = *itr_sig;
    }
    DEBUG_VALUE(itr_buf - buffer.begin());

    DEBUG_MESSAGE(" --- Copy right padding (mirrored inverse)");
    DEBUG_VALUE(itr_buf - buffer.begin());

    // Fill remaining padding with negative signal
    for(itr_sig = signal.end() - 2;
        itr_buf != buffer.end();
        ++itr_buf, --itr_sig)
      *itr_buf = -*itr_sig;
    DEBUG_VALUE(itr_buf - buffer.begin());

  };
};


