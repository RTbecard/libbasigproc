#include <vector>
#include <map>
#include <complex>
#include <fftw3.h>
#include <numeric>
#include <eigen3/Eigen/Core>

#include "../include/libbasigproc.h"
#include "./macros.h"

using std::vector;
using std::complex;

// Setup type aliases
using cmplx = complex<double>;
using vcmplx = vector<complex<double>>;
using vdbl = vector<double>;

using Eigen::MatrixXd;
using Eigen::VectorXcd;
using std::map;

namespace basigproc{

  std::map<std::string, MatrixXd> spectral_ellipse(vector<VectorXcd> &x){

    // Check that all sub vectors are the same length
    for(auto i = x.begin() + 1; i != x.end(); ++i)
      if(x.begin()->size() != i->size())
        throw std::invalid_argument("[libbasigproc::directional_spectrum] x and y must be the same length.");

    int n_dim = x.size();
    int n = x[0].size();

    // Init output object
    std::map<std::string, MatrixXd> ret {
      {"major", MatrixXd(n, n_dim)}, 
        {"minor", MatrixXd(n, n_dim)}};

    // Make references to output matricies
    MatrixXd &major = ret["major"];
    MatrixXd &minor = ret["minor"];

    double phase[3], mag[3], t_0, t_1;

    double half_pi = M_PI*0.5;

    Eigen::VectorXd c_1(n_dim), c_2(n_dim), v_1(n_dim), v_2(n_dim);

    for(int i = 0; i != n; ++i){

      // Get phase and magnitude for complex axis values
      for(int i_dim = 0; i_dim != n_dim; ++i_dim){
        // We have to transform out spectrogram values from power back to root
        // power
        phase[i_dim] = std::arg(x[i_dim][i]);
        mag[i_dim] = std::abs(x[i_dim][i]);
      }
      // Get conjugate axes
      for(int i_dim = 0; i_dim != n_dim; ++i_dim){
        c_1[i_dim] = mag[i_dim] * std::sin(phase[i_dim]);
        c_2[i_dim] = mag[i_dim] * std::cos(phase[i_dim]);
      }
      // Find ellipse verticies (points matching with the major and minor axes)
      t_0 = 0.5 * std::atan(
          (2 * c_1.dot(c_2)) / 
          (c_1.squaredNorm() - c_2.squaredNorm())
          );
      t_1 = t_0 + half_pi;

      // Extract tangent verticies of major/minor axes
      v_1 = c_1*std::cos(t_0) + c_2*std::sin(t_0);
      v_2 = c_1*std::cos(t_1) + c_2*std::sin(t_1);

      if(v_1.norm() > v_2.norm()){
        major.row(i) = v_1;
        minor.row(i) = v_2;
      }else{
        minor.row(i) = v_1;
        major.row(i) = v_2;
      }

    } 

    return ret;

  }
  map<std::string, MatrixXd> directional_spectrogram_xy(
      basigproc::Spectrogram &spec_x, basigproc::Spectrogram &spec_y){

    // Get time and frequency bins
    int n_time = spec_x.n_windows;
    int n_freq = spec_x.n_freq;

    if(!spec_x.parameter_match(spec_y))
      throw std::invalid_argument("spec_x and spec_y do not have matching spectrogram parameters.");

    // Init output matrices

    // Init output map object.
    map<std::string, MatrixXd> ret = {
      {"bearing", MatrixXd(n_freq, n_time)},
      {"eccentricity", MatrixXd(n_freq, n_time)}};
   
    // Make references to output matricies
    MatrixXd &bearing_xy = ret["bearing"];
    MatrixXd &eccentricity = ret["eccentricity"];

    // Temp container for holding ellipse vectors
    map<std::string, MatrixXd> ellips;

    // Temporary vectors for holding spectrogram window segments
    VectorXcd vec_x, vec_y;
    std::vector<VectorXcd> axes(2);

    MatrixXd *ellips_major;
    MatrixXd *ellips_minor;

    for(int i_time = 0; i_time != n_time; ++i_time){
      // Get spectral ellipses (vectors holding the major and minor semi-axis)
      axes = {
        spec_x.root_power_spectral_density(i_time),
        spec_y.root_power_spectral_density(i_time)};
      ellips = basigproc::spectral_ellipse(axes);
      ellips_major = &ellips["major"];
      ellips_minor = &ellips["minor"];

      double major_norm_sq, minor_norm_sq;
      // Extract xy bearing
      for(int i_freq = 0; i_freq != n_freq; ++i_freq){
        bearing_xy(i_freq,i_time) = atan2((*ellips_major)(i_freq,1), (*ellips_major)(i_freq,0));
        major_norm_sq = ellips_major->row(i_freq).squaredNorm();
        minor_norm_sq = ellips_minor->row(i_freq).squaredNorm();
        eccentricity(i_freq,i_time) = sqrt(1 - (minor_norm_sq / major_norm_sq));
      }
    }

    return ret;
  }

  map<std::string, MatrixXd> directional_spectrogram_xyz(
      basigproc::Spectrogram &spec_x, basigproc::Spectrogram &spec_y,
      basigproc::Spectrogram &spec_z){

    // Get time and frequency bins
    int n_time = spec_x.n_windows;
    int n_freq = spec_x.n_freq;

    if(!spec_x.parameter_match(spec_y) || !spec_x.parameter_match(spec_z))
      throw std::invalid_argument("spec_x, spec_y, and spec_z do not have matching spectrogram parameters.");

    // Init output map object.
    map<std::string, MatrixXd> ret = {
      {"bearing", MatrixXd(n_freq, n_time)},
      {"vertical_angle", MatrixXd(n_freq, n_time)},
      {"eccentricity", MatrixXd(n_freq, n_time)}};

    // Init output matrices
    MatrixXd &bearing_xy = ret["bearing"];
    MatrixXd &vertical_angle = ret["vertical_angle"];
    MatrixXd &eccentricity = ret["eccentricity"];

    map<std::string, MatrixXd> ellips;
    
    // Temporary vectors for holding spectrogram window segments
    VectorXcd vec_x, vec_y;
    std::vector<VectorXcd> axes(2);

    for(int i_time = 0; i_time < n_time; ++i_time){
      // Get spectral ellipses (vectors holding the major and minor semi-axis)
      axes = {
        spec_x.root_power_spectral_density(i_time),
        spec_y.root_power_spectral_density(i_time)};
      ellips = basigproc::spectral_ellipse(axes);

      double major_norm_sq, minor_norm_sq;
      // Extract xy bearing
      for(int i_freq = 0; i_freq < n_freq; ++i_freq){
        bearing_xy(i_freq,i_time) = atan2(ellips["major"](i_freq,1), ellips["major"](i_freq,0));
        vertical_angle(i_freq,i_time) = sin(ellips["major"](i_freq, 3));
        major_norm_sq = ellips["major"].row(i_freq).squaredNorm();
        minor_norm_sq = ellips["minor"].row(i_freq).squaredNorm();
        eccentricity(i_freq,i_time) = sqrt(1 - (minor_norm_sq / major_norm_sq));
      }
    }

    return ret;
  }
};
