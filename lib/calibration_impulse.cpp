
#include <vector>
#include <complex>
#include <fftw3.h>
#include <numeric>

#include "eigen3/Eigen/Core"
#include "../include/libbasigproc.h"
#include "./macros.h"

using std::vector;
using std::complex;
using std::exp;

// Setup type aliases
using cmplx = complex<double>;
using vcmplx = vector<complex<double>>;
using vdbl = vector<double>;

namespace basigproc {

  vdbl create_calibration_impulse(vdbl frequency_response, impulse_type type){

    // One-sided spectrum is assumed to hold DC and nyquest frequencies
    // i.e. we are expecting an even numbered two-sided frequency response

    // length of one-sided spectrum
    int n_fft_one = frequency_response.size();
    // length of two-sided spectrum
    int n_fft_two = (n_fft_one-1)*2;
    // Index of nyquest frequency
    int idx_fft_nyq = n_fft_one - 1;

    vcmplx freq_response(n_fft_one);
    vdbl impulse_response(n_fft_two);

    // create two-sided spectrum
    if(type == impulse_type::IMPULSE_MAGNITUDE){

      // Copy magnitude calibration values
      auto i_one_out = freq_response.begin();
      for(auto i_one_in = frequency_response.begin(); i_one_in < frequency_response.end(); ++i_one_in, ++i_one_out){
        *i_one_out = complex<double>(*i_one_in, 0); 
      }
      //Set DC component to 0
      freq_response[0] = complex<double>(0,0);
      /* freq_response[idx_fft_nyq] = complex<double>(*spectrum.end(),0); */
      DEBUG_VECTOR(freq_response.begin(), freq_response.end());

    }else{

      // Copy phase calibration values
      auto i_one_out = freq_response.begin();
      for(auto i_one_in = frequency_response.begin(); i_one_in < frequency_response.end(); ++i_one_in, ++i_one_out){
        *i_one_out = exp(complex<double>(0,*i_one_in)); 
      }
      // DC and nyquest must have no imaginary part (to be symmetrical)
      freq_response[0] = complex<double>(1,0);
      freq_response[idx_fft_nyq] = complex<double>(1,0);
      DEBUG_VECTOR(freq_response.begin(), freq_response.end());
    }

    // Convert to time domain
    fftw_plan plan = fftw_plan_dft_c2r_1d(
        n_fft_two, reinterpret_cast<fftw_complex*>(freq_response.data()), 
        impulse_response.data(), FFTW_ESTIMATE);
    fftw_execute(plan);
    fftw_destroy_plan(plan);
    
    // Scale fft output
    for(auto i = impulse_response.begin(); i != impulse_response.end(); ++i)
      *i /= n_fft_two;

    // Shift impulse response
    vdbl impulse_response_shift(n_fft_two);
    vdbl::iterator i_in;
    vdbl::iterator i_out = impulse_response_shift.begin();
    for(i_in = impulse_response.begin() + floor(n_fft_two/2);
        i_in != impulse_response.end();
        ++i_in, ++i_out)
      *i_out = *i_in;
    
    for(i_in = impulse_response.begin();
        i_out != impulse_response_shift.end();
        ++i_in, ++i_out)
      *i_out = *i_in;

    return impulse_response_shift;

  }

};
