#include <vector>
#include <complex>
#include <fftw3.h>
#include <numeric>
#include <eigen3/Eigen/Core>
#include <vector>

#include "../include/libbasigproc.h"
#include "./macros.h"

using std::vector;
using std::complex;
using cmplx = complex<double>;
using vdbl = vector<double>;
using vcmplx = vector<complex<double>>;

namespace basigproc{

  // Calculate root-mean-square of vector
  double rms(vector<double> x){
    
    // square
    double out = 0;
    for(auto i: x){
      out += pow(i,2);
    }
    // mean
    out /= x.size();
    // root
    out = pow(out, 0.5);

    return out;

  }


  vdbl ricker_wavelet(int n, double sigma){

    vdbl out(n);

    int i_mid = floor(n/2) - 1;
    int t;
    for(int i = 0; i != n; ++i){
      t = i - i_mid;
      out[i] = 
        ( 2 / ( std::pow(double(3) * sigma, 0.5) * pow(M_PI, 0.25) ) ) * 
        ( 1 - pow(t / sigma, 2) ) * 
        exp(-pow(t,2) / (double(2) * pow(sigma,2)));
    }
    return out;
  }

  double psd_to_rpsd(double psd, int n, double fs){

    double scale = fs * double(n);

    // Remove scaling
    double ret = psd * scale  / double(2);
    // Take root power
    ret = pow(ret, 0.5);
    // Reapply scaling
    ret *= double(2) / scale;
    
    return ret;
  }

  double rpsd_to_psd(double rpsd, int n, double fs){

    double scale = fs * double(n);

    // Remove scaling
    double ret = rpsd * scale  / double(2);
    // Take root power
    ret = pow(ret, 2);
    // Reapply scaling
    ret *= double(2) / scale;

    return ret;
  }

  cmplx psd_to_rpsd(cmplx psd, int n, double fs){

    double scale = fs * double(n);

    // Remove scaling
    cmplx ret = psd * scale  / double(2);
    // Take root power
    ret = pow(ret, 0.5);
    // Reapply scaling
    ret *= double(2) / scale;
    
    return ret;
  }

  cmplx rpsd_to_psd(cmplx rpsd, int n, double fs){

    double scale = fs * double(n);

    // Remove scaling
    cmplx ret = rpsd * scale  / double(2);
    // Take root power
    ret = pow(ret, 2);
    // Reapply scaling
    ret *= double(2) / scale;

    return ret;
  }

}
