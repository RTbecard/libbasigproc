#ifndef MACROS_H
#define MACROS_H

#include <iostream>

// Debug macros
#ifdef DEBUG
#define DEBUG_MESSAGE(str) std::cout << " --- " << "libbasigproc::" << __func__ << ":" << str << std::endl
#define DEBUG_VALUE(x) std::cout << " --- " << "libbasigproc::" <<  __func__ << ":" << #x << " = " << x << std::endl
#define DEBUG_VECTOR(start, stop) \
  std::cout << " --- " << "libbasigproc::" <<  __func__ << \
  ":" << "(" << #start << ", " << #stop << ")" << std::endl; \
  for(auto i = start; i != stop; ++i){std::cout << *i << " ";} \
  std::cout << std::endl
#else
#define DEBUG_MESSAGE(str) 
#define DEBUG_VALUE(x) 
#define DEBUG_VECTOR(start, stop)
#endif

#endif
