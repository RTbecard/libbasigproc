#include <vector>
#include <complex>
#include <fftw3.h>
#include <numeric>
#include <eigen3/Eigen/Core>

#include "../include/libbasigproc.h"
#include "./macros.h"

using std::vector;
using std::complex;

// Setup type aliases
using cmplx = complex<double>;
using vcmplx = vector<complex<double>>;
using vdbl = vector<double>;

using Eigen::DenseBase;
using Eigen::MatrixXd;
using Eigen::MatrixXcd;

namespace basigproc {

  Spectrogram::Spectrogram(vdbl &signal, vdbl window, int n_olap, double fs): n_olap(n_olap), fs(fs){

    // Copy over window function
    this->window = window;
    //std::copy(window.begin(), window.end(), back_inserter(this->window));

    // Geenrate complex PSD values
    spec = spectrogram(signal, window, n_olap, fs);

    n_windows = spec.cols();
    n_freq = floor(window.size()/2) + 1;
    // Get frequency resolution
    df = fs/window.size();
    // Get time bin resolution
    dt = (window.size() - n_olap) / fs;

  };

  bool Spectrogram::parameter_match(Spectrogram spec){

    // Compare single valued parameters
    if( 
        this->n_olap != spec.n_olap ||
        this->df != spec.df ||
        this->dt != spec.dt ||
        this->n_freq != spec.n_freq ||
        this->n_windows != spec.n_windows ||
        this->fs != spec.fs ||
        this->window.size() != spec.window.size())
      return false;

    // Compare window functions
    for(auto i = this->window.begin(), j = spec.window.begin();
        i != this->window.end(); ++i, ++j){
      if(*i != *j) return false;
    }

    return(true);
  }

  /* // ------ Return complex magnitude ------ */
  vdbl Spectrogram::magnitude(int time_bin){

    MatrixXcd::ColwiseReturnType::iterator itr = spec.colwise().begin();
    return magnitude(itr);

  };

  vdbl Spectrogram::magnitude(MatrixXcd::ColwiseReturnType::iterator time_bin){

    vdbl ret(n_freq);

    auto i_freq = time_bin->begin();
    for(auto i_ret = ret.begin(); i_ret != ret.end(); ++i_ret, ++i_freq)
      *i_ret = abs(*i_freq);

    return ret;
  };

  MatrixXd Spectrogram::magnitude(){

    // Init return vector
    MatrixXd ret(n_freq, n_windows);
    vdbl tmp(n_freq);

    // Loop over time bins
    auto col_out = ret.colwise().begin();
    auto col_in = spec.colwise().begin();

    Eigen::MatrixXcd::ColXpr::iterator freq_in;
    Eigen::MatrixXd::ColXpr::iterator freq_out;

    for(;col_out != ret.colwise().end(); ++col_out, ++col_in){
      // Loop over freq bins
      freq_in = col_in->begin();
      freq_out = col_out->begin();
      for(; freq_in != col_in->end(); ++freq_in, ++freq_out)
        *freq_out = abs(*freq_in);
    }

    return ret;

  }

  /* // ------ Return root-power spectrum ------ */

  Eigen::VectorXcd Spectrogram::root_power_spectral_density(MatrixXcd::ColwiseReturnType::iterator time_bin){

    Eigen::VectorXcd ret(n_freq);

    auto i_freq = time_bin->begin();
    for(auto i_ret = ret.begin(); i_ret != ret.end(); ++i_ret, ++i_freq)
      *i_ret = psd_to_rpsd(*i_freq, window.size(), fs);
    return ret;
  };
  
  Eigen::VectorXcd Spectrogram::root_power_spectral_density(int time_bin){

    // Convert integer to colwise iterator
    MatrixXcd::ColwiseReturnType::iterator itr = spec.colwise().begin();
    itr += time_bin;

    return root_power_spectral_density(itr);

  };

  MatrixXcd Spectrogram::root_power_spectral_density(){

    // Init return vector
    MatrixXcd ret(n_freq, n_windows);
    vcmplx tmp(n_freq);

    // Loop over time bins
    auto col_out = ret.colwise().begin();
    auto col_in = spec.colwise().begin();

    Eigen::MatrixXcd::ColXpr::iterator freq_in;
    Eigen::MatrixXcd::ColXpr::iterator freq_out;

    for(;col_out != ret.colwise().end(); ++col_out, ++col_in){
      // Loop over freq bins
      freq_in = col_in->begin();
      freq_out = col_out->begin();
      for(; freq_in != col_in->end(); ++freq_in, ++freq_out)
        *freq_out = psd_to_rpsd(*freq_in, window.size(), fs);
    }

    return ret;

  }
  // Average power across time bins (i.e. PSD plot)
  vdbl Spectrogram::welch(){

    // Initalize output vector
    vdbl ret(n_freq);
    std::fill(ret.begin(), ret.end(), 0);

    // Sum PSD values across time bins
    vdbl::iterator i_ret;

    // Loop time bins
    for(auto time_in = spec.colwise().begin(); 
        time_in != spec.colwise().end(); 
        ++time_in){

      /* DEBUG_VALUE(time_in - spec.colwise().begin()) */
      /* DEBUG_VECTOR(time_in->begin(), time_in->end()) */

      // loop frequency bins
      i_ret = ret.begin();
      for(auto freq_in = time_in->begin(); 
          freq_in != time_in->end(); 
          ++freq_in, ++i_ret){

        *i_ret += abs(*freq_in);

      }
      /* DEBUG_VECTOR(ret.begin(), ret.end()) */
    }

    // Normalize (i.e. compute average psd)
    for(auto i_ret = ret.begin(); i_ret != ret.end(); ++i_ret)
      *i_ret /= n_windows;

    return ret;

  };

  double Spectrogram::power(){

    vdbl psd = welch();
    double ret = std::accumulate(psd.begin(), psd.end(), double(0));
    ret *= df;

    return(ret);

  }

  double Spectrogram::energy(){

    return power() * (n_windows*dt);

  }

  vdbl Spectrogram::second(){
    vdbl ret(n_windows);
    for(auto i = ret.begin(); i != ret.end(); ++i)
      *i = (i - ret.begin()) * dt;
    return ret;
  };

  vdbl Spectrogram::frequency(){
    vdbl ret(n_freq);
    for(auto i = ret.begin(); i != ret.end(); ++i)
      *i = (i - ret.begin()) * df;
    return ret;
  }


  // Splot signal into segments, return the windowed PSD for each segment.
  Eigen::MatrixXcd spectrogram(vdbl &signal, vdbl &window, int n_olap, double fs){

    // Get length of window segments
    const int seg_len = window.size();
    // Get number of window segments 
    const int n_windows = std::floor((signal.size() - window.size()) / (seg_len - n_olap));
    // Length of one-sided DFT
    const int n_freq = std::floor(window.size()/2) + 1;
    // Get frequency bin resolution (for converting to spectral density values)
    double df = fs/window.size();
    // Get scaling constant for window function
    double wndw_scale = 1 / rms(window);

    DEBUG_VALUE(seg_len);
    DEBUG_VALUE(n_windows);
    DEBUG_VALUE(n_freq);
    DEBUG_VALUE(df);
    DEBUG_VALUE(wndw_scale);

    // --- Init output structure ---
    /* 
     * Spectrograms will be stored as a vector of complex vectors.
     * spec_out[time_bin][frequency_bin]
     */
    Eigen::MatrixXcd spec_out(n_freq, n_windows);

    // --- Init temporary data containers ---
    // Hold signal chunk to be transformed
    vdbl vec_segment(seg_len);
    // Hold output of one-sided dft operation
    vcmplx vec_dft(n_freq);

    // Setup FFT plan
    fftw_plan fftplan_buffer_t_to_f = fftw_plan_dft_r2c_1d(
        seg_len, vec_segment.data(),
        reinterpret_cast<fftw_complex*>(vec_dft.data()),
        FFTW_ESTIMATE);

    // Iterator for signal vector and window vector
    vdbl::iterator i_sig, i_wndw;
    vcmplx::iterator i_dft;

    // Iteator for Matrix column slice
    Eigen::MatrixXcd::ColXpr::iterator i_dft_out;

    // Loop over window segments
    DEBUG_MESSAGE("Loop over window segments");
    for(int i_seg = 0; i_seg != n_windows; ++i_seg){
      /* DEBUG_VALUE(i_seg) */

      // set iterator for signal
      i_sig = signal.begin() + (seg_len - n_olap) * i_seg;
      // set itertor for window
      i_wndw = window.begin();

      /* DEBUG_VALUE(i_sig - signal.begin()) */

      // Copy signal chunk to buffer
      // We'll window the signal in this step
      for(auto i_seg = vec_segment.begin();
          i_seg != vec_segment.end(); 
          ++i_seg, ++i_sig , ++i_wndw)
        *i_seg = *i_sig * *i_wndw * wndw_scale;

      // Compute one-sided DFT of segment
      fftw_execute(fftplan_buffer_t_to_f);

      // Store result
      i_dft_out = spec_out.col(i_seg).begin();
      for(i_dft = vec_dft.begin();
          i_dft != vec_dft.end(); 
          ++i_dft, ++i_dft_out)
        *i_dft_out = pow(*i_dft,2)*double(2) / (fs * seg_len);
      // Convert to power, normalize wrt frequency bin size.
      // fft scaling constant
      // Multiply by 2 to take into account right-hand side

    }
    fftw_destroy_plan(fftplan_buffer_t_to_f);

    return spec_out;

  }



}
