
set datafile separator ','
set grid

set terminal png

# ------ Magnitude calibration test ------
# Raw signal
set output 'test_analytic_signal.png'
set multiplot layout 2,1 rowsfirst
set xlabel "Sample"
set ylabel "Value"
set title "Signal"
plot "./test_analytic.txt" using 1 with linespoint title "Raw", \
         "./test_analytic_envelope.txt" using 1 with lines title "Envelope"
set title "Instantanious phase"
plot "./test_analytic_phase.txt" using 1 with linespoint title ""
unset multiplot
unset output
