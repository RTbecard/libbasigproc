set datafile separator ','
set grid

set terminal png


set output 'test_spectrogram_magnitude.png'
set xlabel "Time"
set ylabel "Frequency"
set title "Raw signal"
plot '<cat ./test_spec_5s.txt | cut -f1 -d, --complement | tail +2' using 2:1:3 \
matrix with image \
title "PSD"
unset output
