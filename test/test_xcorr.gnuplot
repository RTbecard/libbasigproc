
set datafile separator ','
set grid

set terminal png size 640, 800

# ------ Magnitude calibration test ------
# Raw signal
set output 'test_xcorr.png'
set multiplot layout 4,1 rowsfirst

set xlabel "Sample"
set ylabel "Value"
set title "Signal"
plot "./test_xcorr.txt" using 1 with lines title ""

set xlabel "Sample"
set ylabel "Value"
set title "Template"
plot "./test_xcorr_template.txt" using 1 with lines title ""

set xlabel "Sample"
set ylabel "Value"
set title "Cross correaltion"
plot "./test_xcorr_xcorr.txt" using 1 with lines title ""

set xlabel "Sample"
set ylabel "Value"
set title "Standardized cross correaltion"
plot "./test_xcorr_xcorr_stnd.txt" using 1 with lines title ""

unset multiplot
unset output


set output 'test_xcorr_enveloped.png'
set multiplot layout 4,1 rowsfirst

set xlabel "Sample"
set ylabel "Value"
set title "Signal envelope"
plot "./test_xcorr_env.txt" using 1 with lines title ""

set xlabel "Sample"
set ylabel "Value"
set title "Template envelope"
plot "./test_xcorr_template_env.txt" using 1 with lines title ""

set xlabel "Sample"
set ylabel "Value"
set title "Cross correaltion"
plot "./test_xcorr_env_xcorr.txt" using 1 with lines title ""

set xlabel "Sample"
set ylabel "Value"
set title "Standardized cross correaltion"
plot "./test_xcorr_env_xcorr_stnd.txt" using 1 with lines title ""

unset multiplot
unset output
