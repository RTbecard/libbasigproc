#!/bin/bash

set -e

# Exectute tests
../build_x86_64/src/bintest

# --- Print and open validation plots
# Test 1
gnuplot "./test_calib.gnuplot"
gnuplot "./test_spectrogram.gnuplot"
gnuplot "./test_analytic_signal.gnuplot"
gnuplot "./test_xcorr.gnuplot"
