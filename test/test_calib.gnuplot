set datafile separator ','
set grid

set terminal png

# ------ Magnitude calibration test ------
# Raw signal
set output 'test_calib_mag_raw.png'
set multiplot layout 2,1 rowsfirst
set xlabel "Sample"
set ylabel "Value"
set title "Raw signal"
plot "./test_calib_signal_raw_mag.txt" using 1 with linespoint title ""
set title "Frequnecy response magnitude"
plot "./test_calib_freq_response_mag.txt" using 1 with linespoint title ""
unset multiplot
unset output
# Calibrated signal
set output 'test_calib_mag_calib.png'
set multiplot layout 2,1 rowsfirst
set xlabel "Sample"
set ylabel "Value"
set title "Calibrate signal magnitude"
plot "./test_calib_signal_raw_mag.txt" using 1 with linespoint title "Raw", \
     "./test_calib_signal_calib_mag.txt" using 1 with linespoint title "Calib"
set title "Impulse Response"
plot "./test_calib_impulse_mag.txt" using 1 with linespoint title ""
unset multiplot
unset output

# ------ Phase calibration test ------
# Raw signal
set output 'test_calib_phase_raw.png'
set multiplot layout 2,1 rowsfirst
set xlabel "Sample"
set ylabel "Value"
set title "Raw signal"
plot "./test_calib_signal_calib_phase.txt" using 1 with linespoint title ""
set title "Frequnecy response phase"
plot "./test_calib_freq_response_phase.txt" using 1 with linespoint title ""
unset multiplot
unset output
# Calibrated signal
set output 'test_calib_phase_calib.png'
set multiplot layout 2,1 rowsfirst
set xlabel "Sample"
set ylabel "Value"
set title "Calibrate signal phase"
plot "./test_calib_signal_raw_phase.txt" using 1 with linespoint title "Raw", \
     "./test_calib_signal_calib_phase.txt" using 1 with linespoint title "Calib"
set title "Impulse Response"
plot "./test_calib_impulse_phase.txt" using 1 with linespoint title ""
unset multiplot
unset output


