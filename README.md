## libbasigproc: A C++ library for bioacoustic signal processing

> Note: This library is currently in development

*libbasigproc* is as a low-level set of signal processing functions that is intended to be used by bioacoustics packages written in other programming languages.
It's currently used by the following software:

 - [basigprocR](https://gitlab.com/RTbecard/basigprocr)

*libbasigproc* has the following dependancies: C++17 standard library, [FFTW3](https://www.fftw.org/), and [Eigen3](https://eigen.tuxfamily.org/index.php?title=Main_Page).
Building *libbasigproc* is straight forward.  Simply run `configure && make`.
When Eigen3 and FFTW3 are in non standard locations, you'll have to supply the appropriate flags to `configure`.
Some example build scripts are provided:

 - [script_build.sh](./script_build.sh) is the script we use to build in a shared library on a standard linux distribution.
 This is used to create the binaries in the ubuntu download links below.
 - [script_build_rtools.sh](./script_build_rtools.sh) is used to build *libbasigproc* as a static library from within an Rtools distribution on a windows machine.
 - [script_build_windows.sh](./script_build_windows.sh) is for cross compiling from linux into a static library for windows-based machines using mingw-w64.
 This is used to create the windows binaries in the download links below.
 This script requires the evnironment variable `EIGEN_INCL`, which points to the location of the Eigen3 headers.
 This script also requires a statically compiled version of FFTW3 to be installed in the mingw-w64 distribution.
 - Finally, [script_build_fftw3_static.sh](script_build_fftw3_static.sh) can be used to download and statically compile FFTW3 into a mingw-w64 distribution.

Precompiled versions of these libraries and the source distribution can be downloaded below.  Stable and development refer to the main and dev repository branches, respectively.

### Downloads
 - Stable
   - [Source distribution](https://gitlab.com/RTbecard/libbasigproc/-/jobs/artifacts/main/raw/libbasigproc-0.1.tar.gz?job=build-ubuntu_24.04)
   - [Windows binary (x86_64-w64-mingw32)](https://gitlab.com/RTbecard/libbasigproc/-/jobs/artifacts/main/raw/libbasigproc_x86_64-w64-mingw32.tar.gz?job=build-win64)
   - [Ubuntu 24.04 binary (x86_64)](https://gitlab.com/RTbecard/libbasigproc/-/jobs/artifacts/main/raw/libbasigproc_x86_64.tar.gz?job=build-ubuntu_24.04)

 - Development
   - [Source distribution](https://gitlab.com/RTbecard/libbasigproc/-/jobs/artifacts/dev/raw/libbasigproc-0.1.tar.gz?job=build-ubuntu_24.04)
   - [Windows binary (x86_64-w64-mingw32)](https://gitlab.com/RTbecard/libbasigproc/-/jobs/artifacts/dev/raw/libbasigproc_x86_64-w64-mingw32.tar.gz?job=build-win64)
   - [Ubuntu 24.04 binary (x86_64)](https://gitlab.com/RTbecard/libbasigproc/-/jobs/artifacts/dev/raw/libbasigproc_x86_64.tar.gz?job=build-ubuntu_24.04)
  