#!/bin/bash

set -e -x

# Make buid directory
HOST=$(uname -m)
BUILD_DIR="build_$HOST"

# Remove previously existing build dir
if [ -d $BUILD_DIR ] ; then
    echo "Wiping previous build directory"
    rm -rf $BUILD_DIR
fi

# Create build directory
mkdir $BUILD_DIR; cd $BUILD_DIR

# Make a source archive
../configure && make


