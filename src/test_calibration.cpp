// Load math constants
#define _USE_MATH_DEFINES

#include <iostream>
#include <fstream>
#include <vector>
#include <complex>
#include <cmath>
#include "../include/libbasigproc.h"

using namespace basigproc;

using std::vector;
using std::complex;
using std::ios;
using std::cout;
using vdbl = vector<double>;
using vcmplx = vector<complex<double>>;

void test_calibration(){
  /* --- Make test tone ---
   * two sin waves at 10 Hz and 20 Hz
   * output has a sample rate of 50 Hz
   */

  // ========================= TEST 1 =========================================
 
  // set tone multitone signal properties
  double fs = 200;
  double f_1 = 25;
  double f_2 = 75;
  double f_3 = 10;

  // Write signal to vector
  vdbl vec_sig(200);
  // file handle for writing text files
  std::ofstream f_write;

  for(int i = 0; i < floor(vec_sig.size()/2); ++i){
    // Calculate signal values
    vec_sig[i] = std::sin( i*(f_1/fs)* M_PI * 2 );
  }
  for(int i = floor(vec_sig.size()/2); i != vec_sig.size(); ++i){
    // Calculate signal values
    vec_sig[i] = std::sin( i*(f_2/fs)* M_PI * 2 );
  }

  // init container for frequency reponse
  int n_freq_resp = (1/f_3) * fs;
  vdbl vec_freq_resp(n_freq_resp);
  // Create window function
  vdbl window_hamming = hamming(2*n_freq_resp - 2);
  f_write.open("./test_calib_window.txt", std::ios::out | std::ios::trunc);
  for(auto i: window_hamming) f_write << i << std::endl;
  f_write.close();

  // ------ Test magnitude calibration
  
  // Write signal to csv file
  cout << "Write raw signal to file." << std::endl;
  f_write.open("./test_calib_signal_raw_mag.txt", ios::out | ios::trunc);
  for(auto i: vec_sig) f_write << i << std::endl;
  f_write.close();


  
  // Make frequency response
  cout << "Make magnitude frequency response" << std::endl;
  std::fill(vec_freq_resp.begin(), vec_freq_resp.begin() + std::floor(n_freq_resp/2), sqrt(0.5));
  std::fill(vec_freq_resp.begin() + std::floor(n_freq_resp/2), vec_freq_resp.end(), sqrt(1));
  f_write.open("./test_calib_freq_response_mag.txt", std::ios::out | std::ios::trunc);
  for(auto i: vec_freq_resp) f_write << i << std::endl;
  f_write.close();
  
  /* vdbl vec_impulse_resp(n_freq_resp); */
  /* vec_impulse_resp[0] = sqrt(2); */
  /* fill(vec_impulse_resp.begin()+1, vec_impulse_resp.end(), 0); */

  // Convert to impulse reponse
  cout << "Convert to impulse response " << std::endl;
  vdbl vec_impulse_resp = create_calibration_impulse(vec_freq_resp, impulse_type::IMPULSE_MAGNITUDE);
  // apply window function
  auto i_win = window_hamming.begin();
  for(auto i_imp = vec_impulse_resp.begin(); i_imp != vec_impulse_resp.end(); ++i_imp, ++i_win)
    *i_imp *= *i_win;
  // Write to file
  f_write.open("./test_calib_impulse_mag.txt", ios::out | ios::trunc);
  for(auto i: vec_impulse_resp) f_write << i << std::endl;
  f_write.close();

  // Calibrate signal
  cout << "Calibrate signal magnitude" << std::endl;
  filter(vec_sig, vec_impulse_resp, 128, true);

  // Write calibrated signal to file
  cout << "Write magnitude calibrated signal to file" << std::endl;
  f_write.open("./test_calib_signal_calib_mag.txt", ios::out | ios::trunc);
  for(auto i: vec_sig) f_write << i << std::endl;
  f_write.close();
 


  // ------ Test phase calibration ------

  for(double i = 0; i < vec_sig.size(); ++i){
    // Calculate signal values
    vec_sig[i] = std::sin(i * (f_3/fs) * M_PI * 2 );
  }

  cout << "Write raw signal to file." << std::endl;
  f_write.open("./test_calib_signal_raw_phase.txt", ios::out | ios::trunc);
  for(auto i: vec_sig) f_write << i << std::endl;
  f_write.close();
  
  // Make frequency response
  cout << "Make phase frequency response" << std::endl;
  std::fill(vec_freq_resp.begin(), vec_freq_resp.end(), M_PI/10);
  /* vec_freq_resp[0] = 0; */
  /* std::fill(vec_freq_resp.begin() + 1, vec_freq_resp.end(), -M_PI/2); */
  f_write.open("./test_calib_freq_response_phase.txt", std::ios::out | std::ios::trunc);
  for(auto i: vec_freq_resp) f_write << i << std::endl;
  f_write.close();

  // Convert to impulse reponse
  cout << "Convert to impulse response " << std::endl;
  vec_impulse_resp = create_calibration_impulse(vec_freq_resp, impulse_type::IMPULSE_PHASE);
  /* // apply window function */
  i_win = window_hamming.begin();
  for(auto i_imp = vec_impulse_resp.begin(); i_imp != vec_impulse_resp.end(); ++i_imp, ++i_win)
    *i_imp *= *i_win;
  // Write to file
  f_write.open("./test_calib_impulse_phase.txt", ios::out | ios::trunc);
  for(auto i: vec_impulse_resp) f_write << i << std::endl;
  f_write.close();

  // Calibrate signal
  cout << "Calibrate signal phase" << std::endl;
  filter(vec_sig, vec_impulse_resp, 128, false);

  // Write calibrated signal to file
  cout << "Write phase calibrated signal to file" << std::endl;
  f_write.open("./test_calib_signal_calib_phase.txt", ios::out | ios::trunc);
  for(auto i: vec_sig) f_write << i << std::endl;
  f_write.close();
 
  cout << "Tests complete." << std::endl;

}
