
// Load math constants
#define _USE_MATH_DEFINES

#include <iostream>
#include <fstream>
#include <vector>
#include <complex>
#include <cmath>
#include "../include/libbasigproc.h"

#include "./test_calibration.cpp"
#include "./test_spectrogram.cpp"
#include "./test_analytic_signal.cpp"
#include "./test_xcorr.cpp"

using namespace basigproc;

using std::vector;
using std::complex;
using std::ios;
using std::cout;
using vdbl = vector<double>;
using vcmplx = vector<complex<double>>;

int main(){

  test_calibration();
  test_spectrogram();
  test_analytic_signal();
  test_xcorr();

  // Exit program
  return 0;
}


