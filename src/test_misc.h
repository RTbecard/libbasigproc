#ifndef TEST_MISC_H
#define TEST_MISC_H

// Load math constants
#define _USE_MATH_DEFINES

#include <iostream>
#include <fstream>
#include <vector>
#include <complex>
#include <cmath>
#include <random>
#include "../include/libbasigproc.h"

using namespace basigproc;

using std::vector;
using std::complex;
using std::ios;
using std::cout;
using vdbl = vector<double>;
using vcmplx = vector<complex<double>>;

// Write a vector<double> to text file
void write_vector(vdbl &x, std::string name){

  std::ofstream f_write;
  f_write.open(name, std::ios::out | std::ios::trunc);
  for(auto i: x) f_write << i << std::endl;
  f_write.close();

}

// Create a pure tone signal vector
vdbl pure_tone(int n, double f, double fs){
  
  vdbl out(n);

  for(int i = 0; i < n; ++i){
    // Calculate signal values
    out[i] = std::sin( i*(f/fs)* M_PI * 2 );
  }

  return out;

}

// Create a gaussian noise vector
vdbl white_noise(int n, double sigma){
  
  vdbl out(n);

  std::default_random_engine gen;
  std::normal_distribution<double> gaussian(0, sigma);

  for(int i = 0; i < n; ++i){
    // Calculate signal values
    out[i] = gaussian(gen);
  }

  return out;

}

void add_to(vdbl::iterator in_it_first, vdbl::iterator in_it_last, vdbl::iterator out_it_first){
  
  for(auto itr_in = in_it_first, itr_out = out_it_first;
      itr_in != in_it_last;
      ++itr_in, ++itr_out)
    *itr_out += *itr_in;

}

void add_to(vdbl &vec_in, vdbl::iterator out_it_first){
 
  vdbl::iterator in_it_first = vec_in.begin();
  vdbl::iterator in_it_last = vec_in.end();

  for(auto itr_in = in_it_first, itr_out = out_it_first;
      itr_in != in_it_last;
      ++itr_in, ++itr_out)
    *itr_out += *itr_in;

}

vdbl scale_vector(vdbl x, double scale){
  
  for(auto itr = x.begin(); itr != x.end(); ++itr)
    *itr *= scale; 

  return x;

}

#endif
