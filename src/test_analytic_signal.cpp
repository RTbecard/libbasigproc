// Load math constants
#define _USE_MATH_DEFINES

#include <iostream>
#include <fstream>
#include <vector>
#include <complex>
#include <cmath>
#include "../include/libbasigproc.h"

using namespace basigproc;

using std::vector;
using std::complex;
using std::ios;
using std::cout;
using vdbl = vector<double>;
using vcmplx = vector<complex<double>>;

void test_analytic_signal(){
  /* --- Make test tone ---
   * two sin waves at 10 Hz and 20 Hz
   * output has a sample rate of 50 Hz
   */

  // ========================= TEST 1 =========================================
 
  // set tone multitone signal properties
  double fs = 200;
  double f_1 = 25;
  double f_2 = 75;

  // Write signal to vector
  vdbl vec_sig(200);
  // file handle for writing text files
  std::ofstream f_write;

  // Create base of two tones
  for(int i = 0; i < floor(vec_sig.size()/2); ++i){
    // Calculate signal values
    vec_sig[i] = std::sin( i*(f_1/fs)* M_PI * 2 );
  }
  for(int i = floor(vec_sig.size()/2); i != vec_sig.size(); ++i){
    // Calculate signal values
    vec_sig[i] = std::sin( i*(f_2/fs)* M_PI * 2 );
  }

  // Add ricker wavelets
  vdbl v_rick = ricker_wavelet(20, 4);
  vdbl::iterator itr_sig;

  itr_sig = vec_sig.begin() + 10;
  for(auto itr = v_rick.begin(); itr != v_rick.end(); ++itr, ++itr_sig)
    *itr_sig += *itr * 5;
  
  itr_sig = vec_sig.begin() + 75;
  for(auto itr = v_rick.begin(); itr != v_rick.end(); ++itr, ++itr_sig)
    *itr_sig += *itr * 3;

  itr_sig = vec_sig.begin() + 150;
  for(auto itr = v_rick.begin(); itr != v_rick.end(); ++itr, ++itr_sig)
    *itr_sig += *itr * 4;

  // Write test tone to file
  f_write.open("./test_analytic.txt", std::ios::out | std::ios::trunc);
  for(auto i: vec_sig) f_write << i << std::endl;
  f_write.close();

  // ------ Calculate analytic signal ------
  vcmplx sig_anlyt = analytic_signal(vec_sig);
  vcmplx::iterator itr_sig_anly;

  vdbl sig_envlp(sig_anlyt.size());
  itr_sig_anly = sig_anlyt.begin();
  for(auto itr = sig_envlp.begin(); itr != sig_envlp.end(); ++itr, ++itr_sig_anly)
    *itr = abs(*itr_sig_anly);
  // Write test tone to file
  f_write.open("./test_analytic_envelope.txt", std::ios::out | std::ios::trunc);
  for(auto i: sig_envlp) f_write << i << std::endl;
  f_write.close();

  vdbl sig_phase(sig_anlyt.size());
  itr_sig_anly = sig_anlyt.begin();
  for(auto itr = sig_phase.begin(); itr != sig_phase.end(); ++itr, ++itr_sig_anly)
    *itr = arg(*itr_sig_anly);
  // Write phase to file
  f_write.open("./test_analytic_phase.txt", std::ios::out | std::ios::trunc);
  for(auto i: sig_phase) f_write << i << std::endl;
  f_write.close();

}
