// Load math constants
#define _USE_MATH_DEFINES

#include <iostream>
#include <fstream>
#include <vector>
#include <complex>
#include <cmath>
#include <numeric>
#include "../include/libbasigproc.h"

using namespace basigproc;

using std::vector;
using std::complex;
using std::ios;
using std::cout;
using vdbl = vector<double>;
using vcmplx = vector<complex<double>>;

void test_spectrogram(){

  double fs = 2000;

  // 5 second spectrogram
  cout << "Generate test signal" << std::endl;
  vdbl signal(2000*1);
  for(auto i = signal.begin(); i < signal.end(); ++i)
    *i = std::sin( (signal.begin() - i)*(100/fs)* M_PI * 2 );
  cout << "Signal length: " << signal.size() << std::endl;

  int n_win = 128;
  int n_olap = n_win / 2;
  vdbl win = hamming(n_win);

  cout << "Generate spectrogram" << std::endl;
  Spectrogram spec = Spectrogram(signal, win, n_olap, fs);
  cout << "Spectrogram size: " << spec.spec.rows() << ", " <<spec.spec.cols() << std::endl;

  cout << "Write raw signal to file." << std::endl;
  // Write spectrogram to file
  std::ofstream f_write;
  f_write.open("./test_spec_5s.txt", ios::out | ios::trunc);
  // Write frequency headers
  for(auto v_freq: spec.frequency())
    f_write << ", " << v_freq;
  f_write << std::endl;
  // Loop time bins
  vdbl spec_time = spec.second();
  vdbl::iterator i_sec = spec_time.begin();
  for(auto i_time = spec.spec.colwise().begin(); i_time != spec.spec.colwise().end(); ++i_time, ++i_sec){
    f_write << *i_sec; 
    for(auto v_freq: spec.magnitude(i_time)){
      f_write << ", " << v_freq;
    }
    f_write << std::endl;
  }
  f_write.close();

  cout << "Welch: "  << std::endl;
  for(auto i: spec.welch()) cout << i << ", "; cout << std::endl;
  cout << "Mean-square pressure time: " << pow(rms(signal), 2) << std::endl;
  cout << "Mean-square pressure freq: " << spec.power() << std::endl;

      // 10 second spectrogram
      // 20 second spectrogram


}
