
// Load math constants
#define _USE_MATH_DEFINES

#include <iostream>
#include <fstream>
#include <vector>
#include <complex>
#include <cmath>
#include "./test_misc.h"
#include "../include/libbasigproc.h"

using namespace basigproc;

using std::vector;
using std::complex;
using std::ios;
using std::cout;
using vdbl = vector<double>;
using vcmplx = vector<complex<double>>;

void test_xcorr(){


  // Make noise tone
  vdbl v_sig = white_noise(200, 0.01);

  // Make Ricker wavelet template
  vdbl v_ricker = basigproc::ricker_wavelet(40, 4);
  // Make it asymmetrical
  double s = 1;
  for(auto itr = v_ricker.rbegin() + floor(v_ricker.size()/2);
      itr != v_ricker.rend(); ++itr){
    *itr *= s; s *= 0.5;
  }
    

  vdbl v_ricker_scale;

  v_ricker_scale = scale_vector(v_ricker, 2);
  add_to(v_ricker_scale, v_sig.begin() + 20);

  v_ricker_scale = scale_vector(v_ricker, 3);
  add_to(v_ricker_scale, v_sig.begin() + 70);
  
  v_ricker_scale = scale_vector(v_ricker, 2);
  add_to(v_ricker_scale, v_sig.begin() + 100);
  
  v_ricker_scale = scale_vector(v_ricker, 0.2);
  add_to(v_ricker_scale, v_sig.begin() + 150);

  write_vector(v_sig, "./test_xcorr.txt");
  write_vector(v_ricker, "./test_xcorr_template.txt");
  vdbl v_ricker_env = v_ricker;
  basigproc::analytic_envelope(v_ricker_env);
  write_vector(v_ricker_env, "./test_xcorr_template_env.txt");

  vdbl v_sig_env = v_sig;
  basigproc::analytic_envelope(v_sig_env);
  write_vector(v_sig_env, "./test_xcorr_env.txt");

  // ------ Run xcorr ------
  int chunk_size = 128;
  std::cout << " === Running xcorr" << std::endl;
  vdbl v_xcorr = basigproc::xcorr(v_sig, v_ricker, chunk_size, false, false);
  write_vector(v_xcorr, "./test_xcorr_xcorr.txt");
  
  std::cout << " === Running standardized jxcorr" << std::endl;
  vdbl v_xcorr_std = basigproc::xcorr(v_sig, v_ricker, chunk_size, false, true);
  write_vector(v_xcorr_std, "./test_xcorr_xcorr_stnd.txt");

  // Run enveliped xcorr
  std::cout << " === Running enveloped xcorr" << std::endl;
  vdbl v_env_xcorr = basigproc::xcorr(v_sig, v_ricker, chunk_size, true, false);
  write_vector(v_env_xcorr, "./test_xcorr_env_xcorr.txt");
  
  std::cout << " === Running standardized enveloped xcorr" << std::endl;
  vdbl v_env_xcorr_std = basigproc::xcorr(v_sig, v_ricker, chunk_size, true, true);
  write_vector(v_env_xcorr_std, "./test_xcorr_env_xcorr_stnd.txt");

}
