#!/bin/bash

# Run doxygen to generate man pages
doxygen Doxyfile

# Without this, will default to tinytex rather than texlive.
# Tinytex is missing a bunch of packages, and wont auto install them
# outside of R.
export PATH=/usr/bin:$PATH
echo "$PATH"

cd documentation/latex
make
