#ifndef LIBBASIGPROC_H
#define LIBBASIGPROC_H

#include <complex>
#include <fftw3.h>
#include <vector>
#include<map>
#include <eigen3/Eigen/Core>

// Convert to frequency domain
namespace basigproc
{

  enum signal_type {SIGNAL_RAW = 0, SIGNAL_CALIB = 1};
  
  enum impulse_type {IMPULSE_MAGNITUDE = 0, IMPULSE_PHASE = 1};
  
  // Class for holding metadata about signal
  class SignalMeta
  {
    public:
      // --- Members ---
      // Channels indicies
      int chan_x, chan_y, chan_z, chan_o;
      // Indicates if the signal has been calibrated
      int calibrated;
      // Number of samples (per channel)
      int n;
      // Sample rate
      double fs;
  };

  // Class for holding time-domain signals
  class SignalTime: public SignalMeta
  {
    public:
      // --- Constructor ---
      SignalTime(int chan_x, int chan_y, int chan_z, int chan_o, 
          double fs, std::vector<double> &dat, int calibrated);
      
      // --- Members ---
      std::vector<double> &data;
  };

  // Class for holding frequency domain signals
  // Here, we hold the 1-sided spectrum
  class SignalFreq: public SignalMeta
  {
    public:
      // --- Constructor ---
      SignalFreq(int chan_x, int chan_y, int chan_z, int chan_o, 
          int n, double fs, std::vector<fftw_complex> &data, int calibrated);

      // --- Members ---
      std::vector<fftw_complex> &data;
  };

  // ------ Signal buffer class ------
  
  /** 
   * Class for processing chuncks of a signal in sequence. 
   *
   * This class will take a signal and copy chunks of it at a time into a
   * smaller buffer vector.  After modifications have been done to this buffer,
   * the `next()` function will copy the modified buffer data back into the
   * origional signal vector.
   *
   * `n_pad` allows these buffers to be padded on either side.  The padded
   * samples will not be copied back into the origional signal.  This padding is
   * useful for filter functions, where we need data before and after the signal
   * to apply them correctly.
   *
   * The function `next()` will return `false` when the entire signal has
   * finished buffering, allowing its use as a condition in a while loop.
   *
   * After each call to `next()`, the contents of `&buffer` can be modified.
   *
   */
  class SignalBuffer{

    public:
      // Fields
      std::vector<double> &signal;
      std::vector<double> &buffer;
      std::vector<double> buffer_intr;
      const int n_pad;
      int idx_chunk;
      int n_chunk;
      int size_chunk;
      int size_chunk_inner;

      // Indexing objects
      int idx_chunk_buf_start;
      int idx_chunk_buf_end;
      int idx_chunk_bufintr_start;
      int idx_chunk_bufintr_end;
      typename std::vector<double>::iterator itr_sig;
      /* int idx_sig_end; */
      typename std::vector<double>::iterator itr_buf;

      // Constructor
      SignalBuffer(std::vector<double> &signal, std::vector<double> &buffer, int n_pad);

      /**
       * Each time this method is called, a chunk of the signal is copied into
       * the buffer, and the previously buffered chunk is copied back to the
       * origional signal vector.
       */
      bool next();

    private:
      void read_signal_chunk();
  };


  // ------ Window functions ------
  std::vector<double> hamming(int n);

  std::vector<double> hann(int n);

  // ------ Freqwuency Domain Analysis ------
  
  /** Compute complex spectrogram from signal
   *
   * This is an internal function used by the Spectrogram class.
   */
  Eigen::MatrixXcd spectrogram(
      std::vector<double> &signal, std::vector<double> &window, int n_olap, double fs);
 

  /**
   * Calculate complex spectrogram
   * 
   * The complex spectrogram consists of overlapping window segments holding the
   * results of a one-sided DFT, which has been scaled into units of power
   * spectral density.
   *
   * The resulting complex spectrogram is further used by a number of different
   * analyses.
   */
  class Spectrogram {

    public:
      // matrix holding complex spectrogram
      Eigen::MatrixXcd spec;
      // window
      std::vector<double> window;
      // Overlap
      int n_olap; /**< Number of overlapping samples between window segments */
      double fs; /**< Sample rate of the signal. */
      double df; /**< Size of frequency bins (Hz). */
      double dt; /**< Size of time bins (seconds). */
      int n_freq; /**< Number of frequency bins */
      int n_windows; /**< Numbeer of window segments */

      // Constructor
      Spectrogram(std::vector<double> &signal, std::vector<double> window, int n_olap, double fs);

      /** Return `true` if another Spectrogram object has the same signal
       * parameters as this (other than the signal data). */
      bool parameter_match(Spectrogram spec);

      /** Return the complex magnitude of a spectrogram window segment */
      std::vector<double> magnitude(Eigen::MatrixXcd::ColwiseReturnType::iterator time_bin);
      /** Return the complex magnitude of a spectrogram window segment */
      std::vector<double> magnitude(int time_bin);
      /** Return the complex magnitude for the whole spectrogram */
      Eigen::MatrixXd magnitude();
      
    
      /** Return a window segment of the complex spectrogram scaled for
       * root_power */
      Eigen::VectorXcd root_power_spectral_density(Eigen::MatrixXcd::ColwiseReturnType::iterator time_bin);
      /** Return a window segment of the complex spectrogram scaled for
       * root_power */
      Eigen::VectorXcd root_power_spectral_density(int time_bin);
      /** Return the complex spectrogram scaled for root power. This is useful
       * for dirtectional analysis, where the phase can be used to localize a
       * sound source. */
      Eigen::MatrixXcd root_power_spectral_density();

      /** Return the time-averaged PSD values for each frequecny bin */
      std::vector<double> welch();
      /** Calculate the total power of the spectrogram */
      double power();
      /** Calculate the total energy of the spectrogram */
      double energy();

      /** Get labels for time bins (seconds) */
      std::vector<double> second();
      /** Get labels for frequency bins (Hz) */
      std::vector<double> frequency();

  };

    /** 
     * Convolve a signal with an impulse reponse.
     *
     * This is done via multiplication in the frequency domain.  The signal is
     * broken into overlapping chunks and the DFTs of the chunk and impulse
     * response are multiplied with eachother.
     *
     * Set chunk_size to sizes that are highly divisible by 2 for faster
     * processing times.
     *
     * Applying the filter in the frequnecy domain is much more efficient when
     * dealing with large impulse responses, where in the time-domain
     * convolution operations would follow O = n^2 complexity wrt the length of
     * the impulse response.
     *
     * When filtfilt is true, the signal is convolved a second time with the
     * time-reversed impulse reponse.  These two convolution operations negate
     * the phase changes, resulting in a signal that has the same phase as the
     * origional.  Set filtfilt true when calibrating for the magnitude of
     * the frequency response and false when calibrating the phase.
     */
  void filter(std::vector<double> &signal, 
      std::vector <double> &impulse, int chunk_size, 
      bool filtfilt = false);

  // ------ Calibration functions ------
  std::vector<double> create_calibration_impulse(
      std::vector<double> frequency_response, impulse_type type);

  // ------ Directional spectrogram ------
  /** Return the major and minor semi-axes of the spectral ellipses
   * @param[in] &x A vector holding two or three complex DFT signals. */
  std::map<std::string, Eigen::MatrixXd> spectral_ellipse(std::vector<Eigen::VectorXcd> &x);
  
  std::map<std::string, Eigen::MatrixXd> directional_spectrogram_xy(
      basigproc::Spectrogram &spec_x, basigproc::Spectrogram &spec_y);
  
  std::map<std::string, Eigen::MatrixXd> directional_spectrogram_xyz(
      basigproc::Spectrogram &spec_x, basigproc::Spectrogram &spec_y,
      basigproc::Spectrogram &spec_z);
  
  // ------ Evenloped cross correlation ------
  /** 
   * Return the results of cross correlating the signal with the provided
   * kernel.  When /p standardized is set to `true`, the pearsons correlation
   * coefficent is returned.
   *
   * @param[in] &signal The time-domain signal to run the cross correlation on.
   *
   * @param[in] &kernel_evenlope The the kernel to cross correlate against. */
  std::vector<double> xcorr(std::vector<double> &signal, std::vector<double> kernel, int chunk_size, bool enveloped,  bool standardized = true);

  
  // ------ Miscillanious signal processing math ------
  // Return the root-mean-square of a vector
  double rms(std::vector<double> x);

  /** 
   * Convert a complex frequency domain signal into its analytic signal.
   * The resulting analytic signal is also in the frequency domain.
   * */
  void analytic_signal(std::vector<std::complex<double>> &freq_response);
  
  /** Convert a real time-domain signal into its analytic signal */
  std::vector<std::complex<double>> analytic_signal(std::vector<double> &signal);
  
  /** Convert a real time-domain signal into its analytic envelope */
  void analytic_envelope(std::vector<double> &signal);

  /** Create a Ricker wavelet */
  std::vector<double> ricker_wavelet(int n, double sigma);

  /** Convert from power spectral density to root-power spectral density 
   * root-power spectrum values are handy for directional calculations.
   */
  double psd_to_rpsd(double psd, int n, double fs);
  
  /** Convert from root-power spectral density to power spectral density 
   * root-power spectrum values are handy for directional calculations.
   */
  double rpsd_to_psd(double psd, int n, double fs);
  
  /** Convert from power spectral density to root-power spectral density 
   * root-power spectrum values are handy for directional calculations.
   */
  std::complex<double> psd_to_rpsd(std::complex<double> psd, int n, double fs);
  
  /** Convert from root-power spectral density to power spectral density 
   * root-power spectrum values are handy for directional calculations.
   */
  std::complex<double> rpsd_to_psd(std::complex<double> psd, int n, double fs);

}
#endif
